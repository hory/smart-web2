package cn.com.smart.form.list.bean;

/**
 * 表单列表属性
 * @author lmq
 */
public abstract class AbstractListFieldProp {

    /**
     * 表ID
     */
    private String tableId;
    
    /**
     * 字段ID
     */
    private String fieldId;

    private String fieldName;

    private String title;
    
    /**
     * 是否支持搜索
     */
    private Boolean supportSearch;
    
    /**
     * 是否支持排序
     */
    private Boolean supportSort;
    
    /**
     * 表单插件类型
     */
    private String pluginType;

    public String getTableId() {
        return tableId;
    }

    public void setTableId(String tableId) {
        this.tableId = tableId;
    }

    public String getFieldId() {
        return fieldId;
    }

    public void setFieldId(String fieldId) {
        this.fieldId = fieldId;
    }

    public String getFieldName() {
        return fieldName;
    }

    public void setFieldName(String fieldName) {
        this.fieldName = fieldName;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Boolean getSupportSearch() {
        return supportSearch;
    }

    public void setSupportSearch(Boolean supportSearch) {
        this.supportSearch = supportSearch;
    }

    public Boolean getSupportSort() {
        return supportSort;
    }

    public void setSupportSort(Boolean supportSort) {
        this.supportSort = supportSort;
    }

    public String getPluginType() {
        return pluginType;
    }

    public void setPluginType(String pluginType) {
        this.pluginType = pluginType;
    }
    
}
