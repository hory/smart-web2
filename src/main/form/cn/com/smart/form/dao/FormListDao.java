package cn.com.smart.form.dao;

import org.springframework.stereotype.Repository;

import cn.com.smart.dao.impl.BaseDaoImpl;
import cn.com.smart.form.bean.entity.TFormList;

@Repository
public class FormListDao extends BaseDaoImpl<TFormList> {

}
