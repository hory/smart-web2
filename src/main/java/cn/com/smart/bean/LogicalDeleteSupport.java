package cn.com.smart.bean;

import com.mixsmart.enums.YesNoType;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;

/**
 * 支持逻辑删除
 * @author 乌草坡 2019-09-15
 * @since 1.0
 */
@MappedSuperclass
public abstract class LogicalDeleteSupport extends BaseBeanImpl {

    /**
     * 是否删除标识
     * 1 --- 表示已经删除；0 --- 表示未删除
     */
    private Integer isDelete = YesNoType.NO.getIndex();

    @Column(name = "is_delete")
    public Integer getIsDelete() {
        return isDelete;
    }

    public void setIsDelete(Integer isDelete) {
        this.isDelete = isDelete;
    }

}
